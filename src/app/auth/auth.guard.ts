import { Injectable } from '@angular/core';
import { AuthService, User } from '../auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public userDataSub: Subscription;
  public userData: User;
  constructor(private authService: AuthService, private router: Router) {
    this.userDataSub = this.authService.AuthenticatedUser.subscribe(
      userDataSub => {
        this.userData = userDataSub;
      }
    );
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot
             ): | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
               if (this.authService.isLoggedIn()){
                 return true;
               }
               this.router.navigate(['/login']);
               return false;
             }
}
