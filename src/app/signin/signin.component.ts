import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, User } from '../auth.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public userDataSub: Subscription;
  public userData: User;
  name = '';
  password = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private snackbar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.userDataSub = this.authService.AuthenticatedUser.subscribe(
      userDataSub => {
        this.userData = userDataSub;
      }
    );
  }

  ngOnInit() {}



  openDialog(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '250px',
      data: {name: this.name, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.password = result;
    });
  }

  onSubmit() {
    this.authService.login(this.name, this.password).subscribe(
      () => {
        this.router.navigate(['/feed']);
      },
      error => {
        this.snackbar.open(error);
      }
    );
  }
}
