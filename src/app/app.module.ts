import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { TokenInterceptor } from './token.interceptor';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list'; 
import { MatSidenavModule } from '@angular/material/sidenav'; 
import {MatCheckboxModule} from '@angular/material/checkbox'; 
import { MatDialogModule } from '@angular/material/dialog'; 
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TestComponent } from './test/test.component';
import { FeedComponent } from './feed/feed.component';
import { AuthGuard } from './auth/auth.guard';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatIconModule } from '@angular/material/icon';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { ApiModule, Configuration, ConfigurationParameters} from 'emby';

const appRoutes: Routes = [
  { path: 'login', component: SigninComponent },
  { path: 'test', component: TestComponent },
  { path: 'feed', component: FeedComponent , canActivate: [AuthGuard]},
  { path: '', component: SigninComponent }
];

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    username: 'api',
    password: 'apiqpi',
    apiKeys: {key:'9e98a8ffbe6e4df28fb34bca270aa5f5'}
  }
  return new Configuration(params);
}

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ToolbarComponent,
    TestComponent,
    FeedComponent,
    SidebarComponent,
    LoginDialogComponent
  ],
entryComponents: [ LoginDialogComponent ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    ApiModule.forRoot(apiConfigFactory),
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule,
    MatListModule,
    MatSidenavModule,
    MatButtonModule,
    MatDialogModule,
    MatCheckboxModule,
    LayoutModule,
    MatIconModule
  ],
  exports: [MatFormFieldModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
