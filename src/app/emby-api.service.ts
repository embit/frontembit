import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { environment } from './../environments/environment';

const apiURL = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class EmbyApiService {
  constructor(private http: HttpClient) {}

  fetchMovies() {
    return this.http.get<any>(`${apiURL}/movie`);
  }
}
