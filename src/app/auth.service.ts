import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BehaviorSubject, of } from 'rxjs';
import { catchError, tap, mapTo } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt'
import { environment } from './../environments/environment';

export interface User {
  id: string;
  isLoggedIn: boolean;
}

export class Tokens {
  jwt: string;
  refresh: string;
}

const UNLOGGED_USER = {
  isLoggedIn: false
} as User;

const apiURL = environment.apiUrl + '/embit';
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  protected authenticatedUser: BehaviorSubject<User>;
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  constructor(private http: HttpClient) {
    this.authenticatedUser = new BehaviorSubject<User>(UNLOGGED_USER);
  }

  login(_name: string, _password: string): Observable<User> {
    const user = {
      login: _name,
      password: _password
    };
    return this.http
    .post(
      `${apiURL}/login`,
      user,
    )
    .pipe(catchError(this.handleError))
    .pipe(
      tap((tokens: Tokens) => this.doLoginUser(tokens)),
        map(userJson => {
        const newUser = {
          isLoggedIn: true,
          id: userJson['_id']
        } as User;
        this.authenticatedUser.next(newUser);
        return newUser;
      })
    );
  }

  register(_name: string, _password: string): Observable<User> {
    const user = {
      login: _name,
      password: _password
    };
    return this.http
    .post(
      `${apiURL}/register`,
      user,
    )
    .pipe(catchError(this.handleError))
    .pipe(
      map(userJson => {
        const newUser = {
          isLoggedIn: true,
          id: userJson['_id']
        } as User;
        this.authenticatedUser.next(newUser);
        return newUser;
      })
    );
  }

  logout() {
    return this.http.post<any>(`${apiURL}/logout`, {
      'refreshToken': this.getRefreshToken()
    }).pipe(
    tap(() => this.doLogoutUser()),
      mapTo(true),
    catchError(error => {
      alert(error.error);
      return of(false);
    }));
  }

  refreshToken() {
    return this.http.post<any>(`${apiURL}/embit/refresh`, {
      'refreshToken': this.getRefreshToken()
    }).pipe(tap((tokens: Tokens) => {
      this.storeJwtToken(tokens.jwt);
    }));
  }

  isLoggedIn() {
    const jwtHelper = new JwtHelperService();
    console.log('loogin', !jwtHelper.isTokenExpired(this.getJwtToken()) || !!this.getRefreshToken());
    return (!jwtHelper.isTokenExpired(this.getJwtToken()) || !!this.getRefreshToken());
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error.error.message);
  }

  private doLoginUser(tokens: Tokens) {
    this.storeTokens(tokens);
  }

  public doLogoutUser() {
    this.removeTokens();
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refresh);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }

  get AuthenticatedUser() {
    return this.authenticatedUser.asObservable();
  }
}
