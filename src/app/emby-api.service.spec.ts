import { TestBed } from '@angular/core/testing';

import { EmbyApiService } from './emby-api.service';

describe('EmbyApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmbyApiService = TestBed.get(EmbyApiService);
    expect(service).toBeTruthy();
  });
});
