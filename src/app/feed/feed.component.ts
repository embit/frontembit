import { Component, OnInit } from '@angular/core';
import { AuthService, User } from '../auth.service';
import {MatCheckbox} from '@angular/material/checkbox'; 
import { EmbyApiService } from '../emby-api.service';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})

export class FeedComponent implements OnInit {
  public userData: User;
  public movies = [];

  constructor(private authService: AuthService,
             private embyService: EmbyApiService) {
    this.authService.AuthenticatedUser.subscribe(
      userData => { this.userData = userData; }
    );
  }

  ngOnInit() {
    this.fetchMovies();
  }

  fetchMovies() {
    this.embyService.fetchMovies().subscribe((movies) => {
      this.movies = movies;
    }, (err) => {
      console.error(err)
    })
  }
}
